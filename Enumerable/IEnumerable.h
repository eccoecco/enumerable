/*

IEnumerable.h

A C++ implementation of C#'s IEnumerable<T> in terms of syntax, and
functionality.

Not all features will be implemented, and some leaky abstraction will
occur, but hopefully most things will Just Work.

One of the aims is to do as much compile time determinations as
possible. so this is very meta template programming based.

*/

#pragma once

#include <functional>
#include <list>
#include <vector>
#include <type_traits>

namespace Collections
{

#pragma region IEnumerableWorker<T, U>

    // This is the template which does all the heavy lifting for the enumerable.
    // It is not designed to be easily directly instantiated - but instead, it is expected
    // that this is instantiated by helper functions.  This is because one of the template
    // arguments is a lambda function type.
    //   The iterator function has a specific signature.  It must be callable like so:
    //     tData *pNextData = IteratorFunction();
    //   pNextData will be the next data element, or nullptr if the end of the sequence has
    // been reached.
    template <typename tData, typename tIteratorFunction>
    class IEnumerableWorker
    {
    public:
        IEnumerableWorker(tIteratorFunction iteratorFunction)
            : m_IteratorFunction(iteratorFunction)
        {}

        // Copy constructor should just do a default copy
        IEnumerableWorker(const IEnumerableWorker&) = default;
        // Move operator stays at default
        IEnumerableWorker(IEnumerableWorker&&) = default;

        ~IEnumerableWorker() {}

#pragma region IEnumerableWorker Terminating Functions
        // List of terminating functions - ones that end a chain of IEnumerableWorker<> transforms

        // Returns true if all the elements in a container returns true when passed into compareFunction.
        // Returns true if the container is empty.
        // Returns false if any of the elements returns false when passed into compareFunction.
        template <typename tCompareFunction>
        bool All(tCompareFunction compareFunction) const
        {
            auto iterFunc(IteratorFunction());

            const tData *pData;

            while ((pData = iterFunc()) != nullptr)
            {
                if (!compareFunction(*pData))
                {
                    return false;
                }
            }

            return true;
        }

        // Returns true if the container has elements, false otherwise.
        bool Any() const
        {
            auto iterFunc(IteratorFunction());

            return (iterFunc() != nullptr);
        }

        // Returns true if any element in the container returns true when passed into compareFunction.
        template <typename tCompareFunction>
        bool Any(tCompareFunction compareFunction)
        {
            auto iterFunc(IteratorFunction());

            const tData *pData;

            while ((pData = iterFunc()) != nullptr)
            {
                if (compareFunction(*pData))
                {
                    return true;
                }
            }

            return false;
        }

        // Returns true if the container contains a given element.
        template <typename tCompareData>
        bool Contains(const tCompareData& rData) const
        {
            auto iterFunc(IteratorFunction());

            tData *pData;

            while ((pData = iterFunc()) != nullptr)
            {
                if (*pData == rData)
                {
                    return true;
                }
            }

            return false;
        }

        // Returns the number of elements in the enumerable
        size_t Count() const
        {
            size_t total = 0u;
            auto iterFunc(IteratorFunction());

            tData *pData;

            while ((pData = iterFunc()) != nullptr)
            {
                ++total;
            }

            return total;
        }

        template <typename tCompareFunction>
        size_t Count(tCompareFunction compareFunction) const
        {
            size_t total = 0u;
            auto iterFunc(IteratorFunction());

            tData *pData;

            while ((pData = iterFunc()) != nullptr)
            {
                if (compareFunction(*pData))
                {
                    ++total;
                }
            }

            return total;
        }

        // Returns a pointer to the very first item in the container, nullptr is the sequence is empty.
        auto First() const
        {
            return First([](tData) { return true; });
        }

        // Returns a pointer to the first item in the container that matches the criteria set out in
        // compareFunction.  nullptr is returned if no element matches the given criteria.
        template <typename tCompareFunction>
        tData *First(tCompareFunction compareFunction) const
        {
            auto iterFunc(IteratorFunction());

            tData *pData;

            while ((pData = iterFunc()) != nullptr)
            {
                if (compareFunction(*pData))
                {
                    return pData;
                }
            }

            return pData;
        }

        // Iterates through each item in the enumerable, executing the provided function on
        // each element.  Technically not a very functional thing to do, but this is C++,
        // and we're predominantly an imperative environment, so this is here in the name of
        // pragmatism.
        template <typename tForEachFunction>
        void ForEach(tForEachFunction forEachFunction) const
        {
            // Make a copy, so that the same iterator function can be used multiple times
            // (each time, restarting at the beginning)
            auto iterFunc(IteratorFunction());

            tData *pData;
            while ((pData = iterFunc()) != nullptr)
            {
                forEachFunction(*pData);
            }
        }

        // Iterates through each item in the enumerable, executing the provided function on
        // each elemenet and providing the index of the element.
        template <typename tForEachFunction>
        void ForEachIndex(tForEachFunction forEachFunction) const
        {
            int index = 0;
            auto iterFunc(IteratorFunction());

            tData *pData;
            while ((pData = iterFunc()) != nullptr)
            {
                forEachFunction(*pData, index);
                ++index;
            }
        }

        // Returns a list of pointers to the data
        auto ToList() const
        {
            return ToStdContainer<std::list<tData *>>();
        }

        // Requires a custom comparator, otherwise set will sort by the pointer value.
        // Since set only returns unique values, this implicitly ends up doing a Distinct() for you.
        auto ToSet() const
        {
            typedef std::set<tData *, std::function<bool(tData *, tData *)>> tSetContainer;
            return ToStdContainer<tSetContainer>(tSetContainer([](tData *a, tData *b) { return *a < *b; }));
        }

        // Ditto as ToSet(), with the exception that this is multiset, and so can contain duplicate values.
        auto ToMultiSet() const
        {
            typedef std::multiset<tData *, std::function<bool(tData*, tData *)>> tMultisetContainer;
            return ToStdContainer<tMultisetContainer>(tMultisetContainer([](tData *a, tData *b) { return *a < *b; }));
        }

        // Converts the data type to an std container with a push_back() function.
        // Unlike C#, this returns a container with pointers to the data.  C# can
        // return a reference to the original object, but stl containers cannot contain
        // references, so pointers are the best that we can do.
        template <typename tTargetContainer>
        auto ToStdContainer(tTargetContainer container = tTargetContainer()) const
        {
            auto iterFunc(IteratorFunction());
            tData *pData;

            while ((pData = iterFunc()) != nullptr)
            {
                AddToStdContainer(container, pData, 0);
            }

            return container;
        }

        // Returns a vector of pointers to the original data.
        auto ToVector() const
        {
            return ToStdContainer<std::vector<tData *>>();
        }


#pragma endregion

#pragma region IEnumerableWorker Properties

        // Below is the list of functions that return properties

        // Returns a copy of the iterator function
        tIteratorFunction IteratorFunction() const { return m_IteratorFunction; }

#pragma endregion

#pragma region IEnumerableWorker Transforms
        // Below is the list of functions that transform into another IEnumerableWorker<>

        // Transforms the data type to another data type that already exists.  Use this when the
        // return data type is a member of tData (e.g. tData is a struct, and this is a member of
        // the struct).
        // TODO: Use sfinae to automatically choose between the Select() functions?
        template <typename tSelectFunction>
        auto SelectExisting(tSelectFunction selectFunction) const
        {
            // To help ensure that the item is existing, make sure that the return type of the
            // function is a reference.  This won't stop references of temporary objects being
            // used, but it'll at least provide some warning.
            typedef std::result_of<decltype(selectFunction)(tData&)>::type tNewData;
            static_assert(std::is_reference<tNewData>::value,
                "Return type of selectFunction must be a reference to an existing item.");

            // Remove the reference, so that we can return a nullptr to it, and also create the new
            // IEnumerableWorker() later on
            typedef std::remove_reference<tNewData>::type tNewDataNonRef;

            auto iterFunc(IteratorFunction());

            // Take a copy, not reference, because all variables here go out of scope.
            auto newIterFunction = [=]() mutable
            {
                tData *pData;

                while ((pData = iterFunc()) != nullptr)
                {
                    return &selectFunction(*pData);
                }

                return static_cast<tNewDataNonRef *>(nullptr);
            };

            return IEnumerableWorker<tNewDataNonRef, decltype(newIterFunction)>(newIterFunction);
        }

        // This needs code review!  I'm not sure that this is valid - we're returning a pointer to a
        // locally captured variable for a lambda.
        template <typename tSelectFunction>
        auto SelectNew(tSelectFunction selectFunction) const
        {
            typedef std::result_of<decltype(selectFunction)(tData&)>::type tNewData;
            static_assert(!std::is_reference<tNewData>::value,
                "Return type of selectFunction cannot be a reference to an existing item.");

            // Hmmmm... the iterator function returns a pointer to an element, but since the select
            // function returns a temporary object, we can't just point things at the temporary object,
            // so we have to keep a copy of it somewhere.  Hence, a captured lambda variable.
            //   I wonder if there's a better way?  Possibly return the instance directly (that would
            // mean we don't need tNewData to have a default constructor, or an assignment operator either),
            // but then... how do you indicate end of array?  You could throw an exception, but end of
            // array is not particularly exceptional.  It's expected.  Hm.
            // TODO: ^ Make better.
            tNewData tempData;

            auto iterFunc(IteratorFunction());

            auto newIterFunction = [=]() mutable
            {
                tData *pData;

                while ((pData = iterFunc()) != nullptr)
                {
                    // CODE REVIEW HERE: This is a locally captured data for a mutable lambda, so its
                    // lifetime is the same as the lambda's, but can we return the point to this?
                    tempData = selectFunction(*pData);
                    return &tempData;
                }

                return static_cast<tNewData *>(nullptr);
            };

            return IEnumerableWorker<tNewData, decltype(newIterFunction)>(newIterFunction);
        }

        template <typename tCompareFunction>
        auto Where(tCompareFunction whereFunction) const
        {
            auto iterFunc(IteratorFunction());

            auto newIterFunction = [=]() mutable
            {
                // Compiler error?
                // while ((tData *pData = iterFunc()) != nullptr)

                tData *pData;

                while ((pData = iterFunc()) != nullptr)
                {
                    if (whereFunction(*pData))
                    {
                        return pData;
                    }
                }

                return pData;
            };

            return IEnumerableWorker<tData, decltype(newIterFunction)>(newIterFunction);
        }

#pragma endregion

#pragma region IEnumerableWorker Private
    private:
        // You cannot create a worker without an iterator function
        IEnumerableWorker() = delete;
        // While technically possible, denied by design
        IEnumerableWorker& operator=(const IEnumerableWorker&) = delete;
        // Also denied by design.
        bool operator==(const IEnumerableWorker&) = delete;

        // The iterator function.  Returns a type of tData *.
        // nullptr is returned if the end of the container has been reached,
        // else a valid pointer to a type of tData.
        //   Try not to access this directly - instead, use IteratorFunction()
        // so that you get a copy of the iterator function, not the original.
        tIteratorFunction m_IteratorFunction;

#pragma region Helper functions to add to standard containers

        template <typename tTargetContainer, typename tContainerData, typename = decltype(std::declval<tTargetContainer>().push_back(std::declval<tContainerData>()))>
        void AddToStdContainer(tTargetContainer& rContainer, const tContainerData& rData, int) const
        {
            rContainer.push_back(rData);
        }

        template <typename tTargetContainer, typename tContainerData, typename = decltype(std::declval<tTargetContainer>().insert(std::declval<tContainerData>()))>
        void AddToStdContainer(tTargetContainer& rContainer, const tContainerData& rData, long) const
        {
            rContainer.insert(rData);
        }

#pragma endregion

#pragma endregion

    };

#pragma endregion IEnumerableWorker

#pragma region Helpers

    template <typename tOriginalContainer>
    auto ToEnumerableIteratorFunction(tOriginalContainer& rContainer)
    {
        auto iter = std::begin(rContainer);
        auto endIter = std::end(rContainer);

        auto iterFunction = [&rContainer, iter, endIter]() mutable
        {
            return (iter == endIter) ? nullptr : &(*(iter++));
        };

        return iterFunction;
    }


    template <typename tOriginalContainer>
    auto ToEnumerable(const tOriginalContainer& rContainer)
    {
        auto iterFunction = ToEnumerableIteratorFunction(rContainer);

        // Remove reference because:
        // 1.) You can't really have a container of references
        // 2.) The STL iterators return reference types, which isn't what we want
        return IEnumerableWorker<std::remove_reference<decltype(*std::begin(rContainer))>::type, decltype(iterFunction)>(iterFunction);
    }

#pragma endregion

#pragma region IEnumerable<T>
    // Bounce IEnumerableWorker through an std::function(), instead of a lambda, so that this can
    // be passed through a simplified signature.  More importantly, this also allows a container
    // or IEnumerableWorker to be passed through to a function as an argument, and relies on the
    // appropriate constructor to do an implicit conversion.
    template <typename tData>
    class IEnumerable : public IEnumerableWorker<tData, std::function<tData *()>>
    {
    public:
        ~IEnumerable() {}

        // Copy and move constructors makes sense
        IEnumerable(const IEnumerable&) = default;
        IEnumerable(IEnumerable&&) = default;

        // Given an existing IEnumerableWorker with the same data type as this enumerable,
        // copy its iterator function into this IEnumerable so that we can reuse it.
        template <typename tOriginalIteratorFunction>
        IEnumerable(const IEnumerableWorker<tData, tOriginalIteratorFunction>& rWorker)
            : IEnumerableWorker(rWorker.IteratorFunction())
        {
        }

        // Given a container, generate a new iterator function.
        template <typename tOriginalContainer>
        IEnumerable(tOriginalContainer& rDataSource)
            : IEnumerableWorker(ToEnumerableIteratorFunction(rDataSource))
        {
        }

    private:
        // Can't create an IEnumerable without a data source
        IEnumerable() = delete;
    };

#pragma endregion

};
