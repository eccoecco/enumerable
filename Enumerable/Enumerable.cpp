// Enumerable.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "IEnumerable.h"

using namespace Collections;

int testVectorSource();
int testListStructSource();
void testAssembly();

int main()
{
    testVectorSource();

    testListStructSource();

    testAssembly();

    std::cout << "Press return to continue...\n";
    std::string ignoredLine;
    std::getline(std::cin, ignoredLine);

    return 0;
}


int testVectorSource()
{
    std::cout << "Testing vector source...\n";

    std::vector<int> a{ 1, 2, 3, 4 };

    auto x = ToEnumerable(a);
    x.ForEach([](int a) { std::cout << "Int: " << a << '\n'; });

    x.Where([](int a) { return (a % 2) == 0; })
        .ForEach([](int a) { std::cout << "Even: " << a << '\n'; });

    std::cout << "First (3): " << *x.Where([](int a) { return a == 3; }).First() << '\n';
    std::cout << "First (4): " << *x.First([](int a) { return a == 4; }) << '\n';

    // TODO: Fix - the iterator function decltype() defaults to const iter instead of non-const
    // ^ This is because I pass in const container& - std::begin() turns that into using const_iterator.
    // TODO: Make const and non-const versions.
    IEnumerable<const int> y(a);

    y.ForEach([](const int a) { std::cout << "y: " << a << '\n'; });

    IEnumerable<const int> z(x.Where([](int a) { return a == 3; }));

    std::cout << "z First (3): " << *z.First() << '\n';

    return 0;
}

struct tTestSource
{
    int a = 1, b = 2, c = 3;

    tTestSource(int x)
        : a(x + 1)
        , b(x + 2)
        , c(x + 3)
    {}
};

struct tSelectNew
{
    int x = 10, y = 11;

    tSelectNew()
    {}

    tSelectNew(int a, int b)
        : x(a)
        , y(b)
    {
    }
};

int testListStructSource()
{
    std::list<tTestSource> testList;
    for (int pushIndex = 0; pushIndex < 5; ++pushIndex)
    {
        testList.push_back(tTestSource(pushIndex * 10));
    }

    auto x = ToEnumerable(testList).SelectExisting([](const tTestSource& a) -> const int& { return a.b; });
    x.ForEach([](int a) { std::cout << "List: " << a << '\n'; });
    //auto z = [](const tTestSource& a) -> const int& { return a.b; };
    //std::result_of<decltype(z)(tTestSource&)>::type aa;

    auto y = ToEnumerable(testList).SelectNew([](const tTestSource& a) { return tSelectNew(a.a, a.b); });
    y.ForEach([](const tSelectNew& rSelect) { std::cout << "New: x, y: " << rSelect.x << ", " << rSelect.y << '\n'; });


    auto z = x.ToSet();
    auto zz = x.ToMultiSet();

    return 0;
}

int countEvenNumbersRangedFor(const std::vector<int>& rVector)
{
    int total = 0;

    for (const auto& rValue : rVector)
    {
        if ((rValue % 2) != 0)
        {
            ++total;
        }
    }

    return total;
}

int countEvenNumbersEnumerableLambda(const std::vector<int>& rVector)
{
    //return ToEnumerable(rVector).Where([](const int& rValue) { return (rValue % 2) != 0; }).Count();

    return ToEnumerable(rVector).Count([](const int& rValue) { return (rValue % 2) != 0; });
}

void testAssembly()
{
    const size_t cTestElements = 100;
    std::vector<int> testVector(cTestElements);

    for (unsigned i = 0; i < testVector.size(); ++i)
    {
        testVector[i] = i + 1;
    }

    std::cout << "Even numbers by ranged for loop: " << countEvenNumbersRangedFor(testVector) << '\n';
    std::cout << "Even numbers by enumerable: " << countEvenNumbersEnumerableLambda(testVector) << '\n';
    std::cout << "Even numbers by enumerable inline: " << ToEnumerable(testVector).Count([](const int& rValue) { return (rValue % 2) != 0; }) << '\n';
}