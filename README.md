# Enumerable

This is a little learning project for me to create an IEnumerable<T> equivalent interface in C++14.

There are a few goals to this project:

* It should be possible to handle STL containers, so that you can write a function:

```
#!c++
  void tClass::Process(IEnumerable<int> dataList);

```

  and be able to write:

```
#!c++
  std::vector<int> vecInt;
  std::list<int> listInt;
  std::set<int> setInt;
  int arrayInt[100];

  // Should all work
  tClass test;
  test.Process(vecInt);
  test.Process(listInt);
  test.Process(setInt);
  test.Process(arrayInt);
```

* For simple expressions which the compiler is fully exposed to, it should to be possible that:
```
#!c++
  std::vector<int> vecInt;

  ToEnumerable(vecInt).Where([](const int& i){ return i < 10; })
```
  will compile to a form that is as close to the range based for loop form as possible.

* It should be able to defer execution until data is needed.  Optional bonus: In certain circumstances, it should auto-cache data.

## Implementation Notes

These are scribbled down as I think of them.