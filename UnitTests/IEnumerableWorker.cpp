#include "stdafx.h"
#include "CppUnitTest.h"

#include "../Enumerable/IEnumerable.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Collections;

namespace UnitTests
{

#pragma region Helper functions used for maps
    int operator%(const std::pair<int, double>& rTestPair, int val)
    {
        return (rTestPair.first % val);
    }

    bool operator<(const std::pair<int, double>& rTestPair, int val)
    {
        return rTestPair.first < val;
    }

    bool operator>(const std::pair<int, double>& rTestPair, int val)
    {
        return rTestPair.first > val;
    }

    bool operator==(const std::pair<int, double>& rTestPair, int val)
    {
        return rTestPair.first == val;
    }
#pragma endregion

#pragma region Misc helpers

    // Specific overload because std::size<std::forward_list>() doesn't work
    template <typename tData>
    size_t ContainerLength(const std::forward_list<tData>& rList)
    {
        size_t totalSize = 0;

        for (const auto& ignored : rList)
        {
            (void)ignored;
            ++totalSize;
        }

        return totalSize;
    }

    template <typename tContainer>
    size_t ContainerLength(const tContainer& rContainer)
    {
        return std::size(rContainer);
    }

#pragma endregion

	TEST_CLASS(IEnumerableWorkerTests)
	{
	public:

#pragma region Tests for: Container -> Enumerable -> std::list, std::vector

#pragma region Template test functions
        // tOriginalContainer contains data of type tData
        // tEnumerableContainer contains data of type tData *
        template <typename tOriginalContainer, typename tEnumerableContainer>
        void CompareContainersByPointer(const tOriginalContainer& a, const tEnumerableContainer& b)
        {
            auto iterA = std::begin(a);
            auto iterB = std::begin(b);
            auto endA = std::end(a);
            auto endB = std::end(b);

            for (int itemIndex = 0; iterA != endA; ++iterA, ++iterB, ++itemIndex)
            {
                Assert::IsTrue(iterB != endB, ToString("Container B ended before container A").c_str());

                // The output container should be a pointer to the original element
                Assert::IsTrue(&*iterA == *iterB, ToString("Container element not pointing to same elements").c_str());
            }

            Assert::IsTrue(iterB == endB, ToString("Container A ended before container B").c_str());
        }

        template <typename tOriginalContainer>
        void TestEnumerableConversionToListAndVector(const tOriginalContainer& rContainer)
        {
            auto testEnumerable = ToEnumerable(rContainer);

            auto outputList = testEnumerable.ToList();
            auto outputVector = testEnumerable.ToVector();

            CompareContainersByPointer(rContainer, outputList);
            CompareContainersByPointer(rContainer, outputVector);
        }
#pragma endregion

#pragma region Container instances of the template functions
		
		TEST_METHOD(ToEnumerable_CArrays_ToListVectorCompare)
		{
            int container[]{ 1, 2, 3, 4, 5 };
            TestEnumerableConversionToListAndVector(container);
        }

        TEST_METHOD(ToEnumerable_StdArrays_ToListVectorCompare)
        {
            std::array<int, 5> container{ 1, 2, 3, 4, 5 };
            TestEnumerableConversionToListAndVector(container);
        }

        TEST_METHOD(ToEnumerable_Vector_ToListVectorCompare)
        {
            std::vector<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableConversionToListAndVector(container);
        }

        TEST_METHOD(ToEnumerable_Deque_ToListVectorCompare)
        {
            std::deque<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableConversionToListAndVector(container);
        }

        TEST_METHOD(ToEnumerable_ForwardList_ToListVectorCompare)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableConversionToListAndVector(container);
        }

        TEST_METHOD(ToEnumerable_List_ToListVectorCompare)
        {
            std::list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableConversionToListAndVector(container);
        }

        TEST_METHOD(ToEnumerable_Set_ToListVectorCompare)
        {
            std::set<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableConversionToListAndVector(container);
        }

        TEST_METHOD(ToEnumerable_Map_ToListVectorCompare)
        {
            std::map<int, double> container{ {1, 1.0}, {2, 2.0}, {3, 3.0}, {4, 4.0}, {5, 5.0} };
            TestEnumerableConversionToListAndVector(container);
        }

        TEST_METHOD(ToEnumerable_MultiSet_ToListVectorCompare)
        {
            std::multiset<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableConversionToListAndVector(container);
        }

        TEST_METHOD(ToEnumerable_MultiMap_ToListVectorCompare)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableConversionToListAndVector(container);
        }
#pragma endregion

#pragma endregion

#pragma region Tests for: Container -> Enumerable -> std::set, std::multiset

#pragma region Template test functions

        template <typename tOriginalContainer, typename tEnumerableContainer>
        void CompareContainersByValue(const tOriginalContainer& a, const tEnumerableContainer& b)
        {
            auto iterA = std::begin(a);
            auto iterB = std::begin(b);
            auto endA = std::end(a);
            auto endB = std::end(b);

            for (int itemIndex = 0; iterA != endA; ++iterA, ++iterB, ++itemIndex)
            {
                Assert::IsTrue(iterB != endB, ToString("Container B ended before container A").c_str());

                // The output container should be a pointer to the original element, and since we want to compare
                // values instead of pointers, we dereference a few times.
                Assert::IsTrue(*iterA == **iterB, ToString("Container element not pointing to same values").c_str());
            }

            Assert::IsTrue(iterB == endB, ToString("Container A ended before container B").c_str());
        }

        template <typename tOriginalContainer>
        void TestEnumerableConversionToSetAndMultiSet(const tOriginalContainer& rContainer)
        {
            // Remove const + reference because we're creating copies of the data
            typedef std::remove_const_t<std::remove_reference_t<decltype(*std::begin(rContainer))>> tData;

            std::set<tData> expectedSet;
            std::multiset<tData> expectedMultiSet;

            auto testEnumerable = ToEnumerable(rContainer);

            testEnumerable.ForEach(
                [&](const tData& rValue)
            {
                expectedSet.insert(rValue);
                expectedMultiSet.insert(rValue);
            });

            auto outputSet = testEnumerable.ToSet();
            auto outputMultiSet = testEnumerable.ToMultiSet();

            CompareContainersByValue(expectedSet, outputSet);
            CompareContainersByValue(expectedMultiSet, outputMultiSet);
        }
#pragma endregion

#pragma region Container instances of the template functions

        TEST_METHOD(ToEnumerable_CArrays_ToSetMultisetCompare)
        {
            int container[]{ 1, 2, 2, 3, 4, 5, 5 };
            TestEnumerableConversionToSetAndMultiSet(container);
        }

        TEST_METHOD(ToEnumerable_StdArrays_ToSetMultisetCompare)
        {
            std::array<int, 7> container{ 1, 2, 2, 3, 4, 5, 5 };
            TestEnumerableConversionToSetAndMultiSet(container);
        }

        TEST_METHOD(ToEnumerable_Vector_ToSetMultisetCompare)
        {
            std::vector<int> container{ 1, 2, 2, 3, 4, 5, 5 };
            TestEnumerableConversionToSetAndMultiSet(container);
        }

        TEST_METHOD(ToEnumerable_Deque_ToSetMultisetCompare)
        {
            std::deque<int> container{ 1, 2, 2, 3, 4, 5, 5 };
            TestEnumerableConversionToSetAndMultiSet(container);
        }

        TEST_METHOD(ToEnumerable_ForwardList_ToSetMultisetCompare)
        {
            std::forward_list<int> container{ 1, 2, 2, 3, 4, 5, 5 };
            TestEnumerableConversionToSetAndMultiSet(container);
        }

        TEST_METHOD(ToEnumerable_List_ToSetMultisetCompare)
        {
            std::list<int> container{ 1, 2, 2, 3, 4, 5, 5 };
            TestEnumerableConversionToSetAndMultiSet(container);
        }

        TEST_METHOD(ToEnumerable_Set_ToSetMultisetCompare)
        {
            std::set<int> container{ 1, 2, 2, 3, 4, 5, 5 };
            TestEnumerableConversionToSetAndMultiSet(container);
        }

        TEST_METHOD(ToEnumerable_Map_ToSetMultisetCompare)
        {
            std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableConversionToSetAndMultiSet(container);
        }

        TEST_METHOD(ToEnumerable_MultiSet_ToSetMultisetCompare)
        {
            std::multiset<int> container{ 1, 2, 2, 3, 4, 5, 5 };
            TestEnumerableConversionToSetAndMultiSet(container);
        }

        TEST_METHOD(ToEnumerable_MultiMap_ToSetMultisetCompare)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableConversionToSetAndMultiSet(container);
        }
#pragma endregion

#pragma endregion

#pragma region Tests for: Container -> Enumerable -> First

#pragma region Template test functions
        template <typename tOriginalContainer, typename tFirstData>
        void TestEnumerableFirst(const tOriginalContainer& rContainer, const tFirstData* pFirst)
        {
            auto testEnumerable = ToEnumerable(rContainer);

            Assert::IsTrue(testEnumerable.First() == pFirst, _T("The first element returned does not match the expected first element"));
            Assert::IsTrue(testEnumerable.First([](const tFirstData&) { return false; }) == nullptr, _T("No elements should have been returned when the criteria constantly returns false"));
        }

        template <typename tOriginalContainer>
        void TestEnumerableNonEmptyFirst(const tOriginalContainer& rContainer)
        {
            TestEnumerableFirst(rContainer, &*std::begin(rContainer));
        }

        template <typename tOriginalContainer>
        void TestEnumerableBothEmptyAndNonEmptyFirst(const tOriginalContainer& rContainer)
        {
            typedef tOriginalContainer::value_type tData;

            TestEnumerableNonEmptyFirst(rContainer);

            tOriginalContainer emptyContainer;
            // Empty containers should always return nullptr
            TestEnumerableFirst(emptyContainer, static_cast<tData *>(nullptr));

            // Okay, those tests use lambdas to First() which always return a constant value.
            // Now let's try iterating through the container, and then using First() to find that
            // specific value.  We do pointer comparisons because ToEnumerable() and First() should
            // *not* be making copies of the data.
            for (const auto& elem : rContainer)
            {
                auto pResult = ToEnumerable(rContainer)
                    .First([&](const tData& rData) { return &rData == &elem; });

                Assert::IsTrue(pResult == &elem, _T("The element returned does not match the expected element"));
            }
        }

#pragma endregion

#pragma region Container instances

        TEST_METHOD(ToEnumerable_CArrays_First)
        {
            int container[]{ 1, 2, 3, 4, 5 };
            TestEnumerableNonEmptyFirst(container);
        }

        TEST_METHOD(ToEnumerable_StdArrays_First)
        {
            std::array<int, 5> container{ 1, 2, 3, 4, 5 };
            TestEnumerableNonEmptyFirst(container);
        }

        TEST_METHOD(ToEnumerable_Vector_First)
        {
            std::vector<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableBothEmptyAndNonEmptyFirst(container);
        }

        TEST_METHOD(ToEnumerable_Deque_First)
        {
            std::deque<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableBothEmptyAndNonEmptyFirst(container);
        }

        TEST_METHOD(ToEnumerable_ForwardList_First)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableBothEmptyAndNonEmptyFirst(container);
        }

        TEST_METHOD(ToEnumerable_List_First)
        {
            std::list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableBothEmptyAndNonEmptyFirst(container);
        }

        TEST_METHOD(ToEnumerable_Set_First)
        {
            std::set<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableBothEmptyAndNonEmptyFirst(container);
        }

        TEST_METHOD(ToEnumerable_Map_First)
        {
            std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableBothEmptyAndNonEmptyFirst(container);
        }

        TEST_METHOD(ToEnumerable_MultiSet_First)
        {
            std::multiset<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableBothEmptyAndNonEmptyFirst(container);
        }

        TEST_METHOD(ToEnumerable_MultiMap_First)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableBothEmptyAndNonEmptyFirst(container);
        }

#pragma endregion

#pragma endregion

#pragma region Tests for: Container -> Enumerable -> ForEach, ForEachIndex

#pragma region Template test functions
        template <typename tOriginalContainer, typename tData = tOriginalContainer::value_type>
        void TestEnumerableForEachAndIndex(const tOriginalContainer& rContainer)
        {
            auto testEnumerable = ToEnumerable(rContainer);

            auto iter = std::begin(rContainer);
            auto endIter = std::end(rContainer);

            testEnumerable.ForEach([&](const tData& rData)
            {  
                Assert::IsTrue(iter != endIter, _T("ForEach() has introduced more elements than was present in the container!"));
                // Check pointer to ensure that we have the same element
                Assert::IsTrue(&rData == &*iter, _T("ForEach() has returned an element that is not the same as the container"));
                ++iter;
            });

            Assert::IsTrue(iter == endIter, _T("ForEach() has returned fewer elements than was in the original container!"));

            iter = std::begin(rContainer);
            int expectedIndex = 0;

            testEnumerable.ForEachIndex([&](const tData& rData, int providedIndex)
            {
                Assert::IsTrue(providedIndex == expectedIndex, _T("ForEachIndex() has provided an unexpected index value."));
                Assert::IsTrue(iter != endIter, _T("ForEachIndex() has introduced more elements than was present in the container!"));
                // Check pointer to ensure that we have the same element
                Assert::IsTrue(&rData == &*iter, _T("ForEachIndex() has returned an element that is not the same as the container"));
                ++iter;
                ++expectedIndex;
            });

            Assert::IsTrue(iter == endIter, _T("ForEachIndex() has returned fewer elements than was in the original container!"));
        }

#pragma endregion

#pragma region Container instances

        TEST_METHOD(ToEnumerable_CArrays_ForEachAndIndex)
        {
            int container[]{ 1, 2, 3, 4, 5 };
            TestEnumerableForEachAndIndex<decltype(container), int>(container);
        }

        TEST_METHOD(ToEnumerable_StdArrays_ForEachAndIndex)
        {
            std::array<int, 5> container{ 1, 2, 3, 4, 5 };
            TestEnumerableForEachAndIndex(container);
        }

        TEST_METHOD(ToEnumerable_Vector_ForEachAndIndex)
        {
            std::vector<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableForEachAndIndex(container);
        }

        TEST_METHOD(ToEnumerable_Deque_ForEachAndIndex)
        {
            std::deque<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableForEachAndIndex(container);
        }

        TEST_METHOD(ToEnumerable_ForwardList_ForEachAndIndex)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableForEachAndIndex(container);
        }

        TEST_METHOD(ToEnumerable_List_ForEachAndIndex)
        {
            std::list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableForEachAndIndex(container);
        }

        TEST_METHOD(ToEnumerable_Set_ForEachAndIndex)
        {
            std::set<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableForEachAndIndex(container);
        }

        TEST_METHOD(ToEnumerable_Map_ForEachAndIndex)
        {
            std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableForEachAndIndex(container);
        }

        TEST_METHOD(ToEnumerable_MultiSet_ForEachAndIndex)
        {
            std::multiset<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableForEachAndIndex(container);
        }

        TEST_METHOD(ToEnumerable_MultiMap_ForEachAndIndex)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableForEachAndIndex(container);
        }

#pragma endregion

#pragma endregion

#pragma region Tests for: Container -> Enumerable -> Where -> std::list

#pragma region Template test functions

        template <typename tOriginalContainer, typename tEnumerableContainer>
        void CompareContainersByContentDirect(const tOriginalContainer& a, const tEnumerableContainer& b)
        {
            auto iterA = std::begin(a);
            auto iterB = std::begin(b);
            auto endA = std::end(a);
            auto endB = std::end(b);

            for (int itemIndex = 0; iterA != endA; ++iterA, ++iterB, ++itemIndex)
            {
                Assert::IsTrue(iterB != endB, ToString("Container B ended before container A").c_str());

                // The output container should be a pointer to the original element, and since we want to compare
                // values instead of pointers, we dereference a few times.
                Assert::IsTrue(*iterA == *iterB, ToString("Container element not pointing to same values").c_str());
            }

            Assert::IsTrue(iterB == endB, ToString("Container A ended before container B").c_str());
        }

        template <typename tOriginalContainer, typename tData = tOriginalContainer::value_type, typename tTestFunction = std::function<bool(const tData&)>>
        void TestEnumerableWhereToList(const tOriginalContainer& rContainer, tTestFunction testFunction = [](const tData& rData) { return (rData % 2) == 0; })
        {
            std::list<const tData *> expectedResult;
            for (const auto& rData : rContainer)
            {
                if (testFunction(rData))
                {
                    expectedResult.push_back(&rData);
                }
            }

            auto iter = std::begin(rContainer);
            auto endIter = std::end(rContainer);

            auto testEnumerable = ToEnumerable(rContainer);
            auto whereResults = testEnumerable.Where(testFunction).ToList();

            CompareContainersByContentDirect(expectedResult, whereResults);
        }

#pragma endregion

#pragma region Container instances

        TEST_METHOD(ToEnumerable_CArrays_WhereToList)
        {
            int container[]{ 1, 2, 3, 4, 5 };
            TestEnumerableWhereToList<decltype(container), int>(container);
        }

        TEST_METHOD(ToEnumerable_StdArrays_WhereToList)
        {
            std::array<int, 5> container{ 1, 2, 3, 4, 5 };
            TestEnumerableWhereToList(container);
        }

        TEST_METHOD(ToEnumerable_Vector_WhereToList)
        {
            std::vector<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableWhereToList(container);
        }

        TEST_METHOD(ToEnumerable_Deque_WhereToList)
        {
            std::deque<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableWhereToList(container);
        }

        TEST_METHOD(ToEnumerable_ForwardList_WhereToList)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableWhereToList(container);
        }

        TEST_METHOD(ToEnumerable_List_WhereToList)
        {
            std::list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableWhereToList(container);
        }

        TEST_METHOD(ToEnumerable_Set_WhereToList)
        {
            std::set<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableWhereToList(container);
        }

        TEST_METHOD(ToEnumerable_Map_WhereToList)
        {
            std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableWhereToList(container);
        }

        TEST_METHOD(ToEnumerable_MultiSet_WhereToList)
        {
            std::multiset<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableWhereToList(container);
        }

        TEST_METHOD(ToEnumerable_MultiMap_WhereToList)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableWhereToList(container);
        }

#pragma endregion

#pragma endregion

#pragma region Tests for: Container -> Enumerable -> Count

#pragma region Template test functions

		template <typename tOriginalContainer, typename tData = tOriginalContainer::value_type, typename tTestFunction = std::function<bool(const tData&)>>
		void TestEnumerableCount(const tOriginalContainer& rContainer, tTestFunction testFunction = [](const tData& rData) { return (rData % 2) == 0; })
		{
            Assert::IsTrue(ContainerLength(rContainer) == ToEnumerable(rContainer).Count(), _T("Unexpected container count!"));

            int filteredCount = 0;

            for (const auto& rData : rContainer)
            {
                if (testFunction(rData))
                    ++filteredCount;
            }

            Assert::IsTrue(filteredCount == ToEnumerable(rContainer).Count(testFunction), _T("Unexpected filtered countainer count!"));
		}

#pragma endregion

#pragma region Container instances

		TEST_METHOD(ToEnumerable_CArrays_Count)
		{
			int container[]{ 1, 2, 3, 4, 5 };
			TestEnumerableCount<decltype(container), int>(container);
		}

		TEST_METHOD(ToEnumerable_StdArrays_Count)
		{
			std::array<int, 5> container{ 1, 2, 3, 4, 5 };
			TestEnumerableCount(container);
		}

		TEST_METHOD(ToEnumerable_Vector_Count)
		{
			std::vector<int> container{ 1, 2, 3, 4, 5 };
			TestEnumerableCount(container);
		}

		TEST_METHOD(ToEnumerable_Deque_Count)
		{
			std::deque<int> container{ 1, 2, 3, 4, 5 };
			TestEnumerableCount(container);
		}

        TEST_METHOD(ToEnumerable_ForwardList_Count)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableCount(container);
        }

		TEST_METHOD(ToEnumerable_List_Count)
		{
			std::list<int> container{ 1, 2, 3, 4, 5 };
			TestEnumerableCount(container);
		}

		TEST_METHOD(ToEnumerable_Set_Count)
		{
			std::set<int> container{ 1, 2, 3, 4, 5 };
			TestEnumerableCount(container);
		}

		TEST_METHOD(ToEnumerable_Map_Count)
		{
			std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
			TestEnumerableCount(container);
		}

		TEST_METHOD(ToEnumerable_MultiSet_Count)
		{
			std::multiset<int> container{ 1, 2, 3, 4, 5 };
			TestEnumerableCount(container);
		}

		TEST_METHOD(ToEnumerable_MultiMap_Count)
		{
			std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
			TestEnumerableCount(container);
		}

#pragma endregion

#pragma endregion

#pragma region Tests for: Container -> Enumerable -> Any/All

#pragma region Template test functions

        template <typename tOriginalContainer, typename tData = tOriginalContainer::value_type>
        void TestEnumerableAnyAll(const tOriginalContainer& rContainer)
        {
            auto testEnumerable = ToEnumerable(rContainer);

            // Test all valid
            Assert::IsTrue(testEnumerable.All([](const tData& rData) { return rData < 6; }), _T("Unexpected result for All() < 6"));
            // Test fails on last item
            Assert::IsFalse(testEnumerable.All([](const tData& rData) { return rData < 5; }), _T("Unexpected result for All() < 5"));
            // Test fails on first item
            Assert::IsFalse(testEnumerable.All([](const tData& rData) { return rData < -1; }), _T("Unexpected result for All() < -1"));
            // Test fails halfway through
            Assert::IsFalse(testEnumerable.All([](const tData& rData) { return rData < 3; }), _T("Unexpected result for All() < 3"));

            // Test true on first item
            Assert::IsTrue(testEnumerable.Any([](const tData& rData) { return rData < 6; }), _T("Unexpected result for Any() < 6"));
            // Test true halfway through
            Assert::IsTrue(testEnumerable.Any([](const tData& rData) { return rData > 3; }), _T("Unexpected result for Any() > 3"));
            // Test true on last item
            Assert::IsTrue(testEnumerable.Any([](const tData& rData) { return rData == 5; }), _T("Unexpected result for Any() == 5"));
            // Test fail
            Assert::IsFalse(testEnumerable.Any([](const tData& rData) { return rData < 0; }), _T("Unexpected result for Any() < 0"));

            Assert::IsTrue(testEnumerable.Any(), _T("Unexpected result for Any()"));

            // Certain types (e.g. arrays) will not be empty, but std::containers will be
            tOriginalContainer emptyContainer{};
            bool expectedResult = ContainerLength(emptyContainer) > 0;

            Assert::IsTrue(expectedResult == ToEnumerable(emptyContainer).Any(), _T("Unexpected result for emptyContainer.Any()"));

        }

#pragma endregion

#pragma region Container instances

        TEST_METHOD(ToEnumerable_CArrays_AnyAll)
        {
            int container[]{ 1, 2, 3, 4, 5 };
            TestEnumerableAnyAll<decltype(container), int>(container);
        }

        TEST_METHOD(ToEnumerable_StdArrays_AnyAll)
        {
            std::array<int, 5> container{ 1, 2, 3, 4, 5 };
            TestEnumerableAnyAll(container);
        }

        TEST_METHOD(ToEnumerable_Vector_AnyAll)
        {
            std::vector<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableAnyAll(container);
        }

        TEST_METHOD(ToEnumerable_Deque_AnyAll)
        {
            std::deque<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableAnyAll(container);
        }

        TEST_METHOD(ToEnumerable_ForwardList_AnyAll)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableAnyAll(container);
        }

        TEST_METHOD(ToEnumerable_List_AnyAll)
        {
            std::list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableAnyAll(container);
        }

        TEST_METHOD(ToEnumerable_Set_AnyAll)
        {
            std::set<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableAnyAll(container);
        }

        TEST_METHOD(ToEnumerable_Map_AnyAll)
        {
            std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableAnyAll(container);
        }

        TEST_METHOD(ToEnumerable_MultiSet_AnyAll)
        {
            std::multiset<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableAnyAll(container);
        }

        TEST_METHOD(ToEnumerable_MultiMap_AnyAll)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableAnyAll(container);
        }

#pragma endregion

#pragma endregion


#pragma region Tests for: Container -> Enumerable -> Contains

#pragma region Template test functions

        template <typename tOriginalContainer, typename tData = tOriginalContainer::value_type>
        void TestEnumerableContains(const tOriginalContainer& rContainer, tData notInContainer = tData())
        {
            auto testEnumerable = ToEnumerable(rContainer);

            // Test once here, just to force the iterator function to go to the back
            // It should be a copy, so the next few tests will work, but if it's not a copy, then we
            // won't be able to find anything either.
            Assert::IsFalse(testEnumerable.Contains(notInContainer), _T("Contains() found something that should not exist"));

            for (const auto& rData : rContainer)
            {
                Assert::IsTrue(testEnumerable.Contains(rData), _T("Contains() unexpectedly returned false"));
            }
        }

#pragma endregion

#pragma region Container instances

        TEST_METHOD(ToEnumerable_CArrays_Contains)
        {
            int container[]{ 1, 2, 3, 4, 5 };
            TestEnumerableContains<decltype(container), int>(container);
        }

        TEST_METHOD(ToEnumerable_StdArrays_Contains)
        {
            std::array<int, 5> container{ 1, 2, 3, 4, 5 };
            TestEnumerableContains(container);
        }

        TEST_METHOD(ToEnumerable_Vector_Contains)
        {
            std::vector<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableContains(container);
        }

        TEST_METHOD(ToEnumerable_Deque_Contains)
        {
            std::deque<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableContains(container);
        }

        TEST_METHOD(ToEnumerable_ForwardList_Contains)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableContains(container);
        }

        TEST_METHOD(ToEnumerable_List_Contains)
        {
            std::list<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableContains(container);
        }

        TEST_METHOD(ToEnumerable_Set_Contains)
        {
            std::set<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableContains(container);
        }

        TEST_METHOD(ToEnumerable_Map_Contains)
        {
            std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableContains(container);
        }

        TEST_METHOD(ToEnumerable_MultiSet_Contains)
        {
            std::multiset<int> container{ 1, 2, 3, 4, 5 };
            TestEnumerableContains(container);
        }

        TEST_METHOD(ToEnumerable_MultiMap_Contains)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestEnumerableContains(container);
        }

#pragma endregion

#pragma endregion

    };
}