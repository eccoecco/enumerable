#include "stdafx.h"
#include "CppUnitTest.h"

#include "../Enumerable/IEnumerable.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Collections;

namespace UnitTests
{
    TEST_CLASS(IEnumerableTests)
    {
    public:

#pragma region Tests for: Non-const Container -> Non-const IEnumerable -> Verify contents

#pragma region Template test functions
        template <typename tOriginalContainer, typename tDataType>
        void TestIEnumerableNonConstInner(tOriginalContainer& rOrigData, IEnumerable<tDataType> enumerableContainer)
        {
            auto iter = std::begin(rOrigData);
            auto endIter = std::end(rOrigData);

            enumerableContainer.ForEach([&](const tDataType& rData)
            {
                Assert::IsTrue(iter != endIter, _T("Original container ended before enumerable container did"));
                // Compare the pointers, because the enumerable container is expected to return the exact same data
                Assert::IsTrue(&*iter == &rData, _T("Enumerable container is not returning the same data as the original container"));
                ++iter;
            });

            Assert::IsTrue(iter == endIter, _T("Original container has more data than enumerable container"));
        }


        template <typename tOriginalContainer, typename tDataType = tOriginalContainer::value_type>
        void TestIEnumerableNonConst(tOriginalContainer& rOrigData)
        {
            // Explicitly call the inner to test the implicit cast from an std::container<> to IEnumerable<>
            TestIEnumerableNonConstInner<tOriginalContainer, tDataType>(rOrigData, rOrigData);
        }

#pragma endregion

#pragma region Container instances of the template functions
        TEST_METHOD(IEnumerable_CArrays_ToNonConstEnumerable)
        {
            int container[]{ 1, 2, 3, 4, 5 };

            TestIEnumerableNonConst<decltype(container), int>(container);
        }

        TEST_METHOD(IEnumerable_StdArrays_ToNonConstEnumerable)
        {
            std::array<int, 5> container{ 1, 2, 3, 4, 5 };

            TestIEnumerableNonConst(container);
        }

        TEST_METHOD(IEnumerable_Vector_ToNonConstEnumerable)
        {
            std::vector<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableNonConst(container);
        }

        TEST_METHOD(IEnumerable_Deque_ToNonConstEnumerable)
        {
            std::deque<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableNonConst(container);
        }

        TEST_METHOD(IEnumerable_ForwardList_ToNonConstEnumerable)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableNonConst(container);
        }

        TEST_METHOD(IEnumerable_List_ToNonConstEnumerable)
        {
            std::list<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableNonConst(container);
        }

        TEST_METHOD(IEnumerable_Set_ToNonConstEnumerable)
        {
            std::set<int> container{ 1, 2, 3, 4, 5 };
            // Have to explicitly specify the type, because set returns iterator to const int
            TestIEnumerableNonConst<decltype(container), const int>(container);
        }

        TEST_METHOD(IEnumerable_Map_ToNonConstEnumerable)
        {
            std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestIEnumerableNonConst(container);
        }

        TEST_METHOD(IEnumerable_MultiSet_ToNonConstEnumerable)
        {
            std::multiset<int> container{ 1, 2, 3, 4, 5 };
            // Have to explicitly specify the type, because multiset returns iterator to const int
            TestIEnumerableNonConst<decltype(container), const int>(container);
        }

        TEST_METHOD(IEnumerable_MultiMap_ToNonConstEnumerable)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestIEnumerableNonConst(container);
        }
#pragma endregion

#pragma endregion

#pragma region Tests for: Const Container -> Cconst IEnumerable -> Verify contents

#pragma region Template test functions
        template <typename tOriginalContainer, typename tDataType>
        void TestIEnumerableConstInner(const tOriginalContainer& rOrigData, IEnumerable<tDataType> enumerableContainer)
        {
            auto iter = std::begin(rOrigData);
            auto endIter = std::end(rOrigData);

            enumerableContainer.ForEach([&](const tDataType& rData)
            {
                Assert::IsTrue(iter != endIter, _T("Original container ended before enumerable container did"));
                // Compare the pointers, because the enumerable container is expected to return the exact same data
                Assert::IsTrue(&*iter == &rData, _T("Enumerable container is not returning the same data as the original container"));
                ++iter;
            });

            Assert::IsTrue(iter == endIter, _T("Original container has more data than enumerable container"));
        }


        template <typename tOriginalContainer, typename tDataType = const tOriginalContainer::value_type>
        void TestIEnumerableConst(const tOriginalContainer& rOrigData)
        {
            // Explicitly call the inner to test the implicit cast from an std::container<> to IEnumerable<>
            TestIEnumerableConstInner<tOriginalContainer, tDataType>(rOrigData, rOrigData);
        }

#pragma endregion

#pragma region Container instances of the template functions
        TEST_METHOD(IEnumerable_CArrays_ToConstEnumerable)
        {
            int container[]{ 1, 2, 3, 4, 5 };

            TestIEnumerableConst<decltype(container), const int>(container);
        }

        TEST_METHOD(IEnumerable_StdArrays_ToConstEnumerable)
        {
            std::array<int, 5> container{ 1, 2, 3, 4, 5 };

            TestIEnumerableConst(container);
        }

        TEST_METHOD(IEnumerable_Vector_ToConstEnumerable)
        {
            std::vector<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableConst(container);
        }

        TEST_METHOD(IEnumerable_Deque_ToConstEnumerable)
        {
            std::deque<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableConst(container);
        }

        TEST_METHOD(IEnumerable_ForwardList_ToConstEnumerable)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableConst(container);
        }

        TEST_METHOD(IEnumerable_List_ToConstEnumerable)
        {
            std::list<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableConst(container);
        }

        TEST_METHOD(IEnumerable_Set_ToConstEnumerable)
        {
            std::set<int> container{ 1, 2, 3, 4, 5 };
            // Have to explicitly specify the type, because set returns iterator to const int
            TestIEnumerableConst(container);
        }

        TEST_METHOD(IEnumerable_Map_ToConstEnumerable)
        {
            std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestIEnumerableConst(container);
        }

        TEST_METHOD(IEnumerable_MultiSet_ToConstEnumerable)
        {
            std::multiset<int> container{ 1, 2, 3, 4, 5 };
            // Have to explicitly specify the type, because multiset returns iterator to const int
            TestIEnumerableConst(container);
        }

        TEST_METHOD(IEnumerable_MultiMap_ToConstEnumerable)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestIEnumerableConst(container);
        }
#pragma endregion

#pragma endregion

#pragma region Tests for: Non-const Container -> const IEnumerable -> Verify contents

#pragma region Template test functions
        template <typename tOriginalContainer, typename tDataType>
        void TestIEnumerableMixedInner(tOriginalContainer& rOrigData, IEnumerable<tDataType> enumerableContainer)
        {
            auto iter = std::begin(rOrigData);
            auto endIter = std::end(rOrigData);

            enumerableContainer.ForEach([&](const tDataType& rData)
            {
                Assert::IsTrue(iter != endIter, _T("Original container ended before enumerable container did"));
                // Compare the pointers, because the enumerable container is expected to return the exact same data
                Assert::IsTrue(&*iter == &rData, _T("Enumerable container is not returning the same data as the original container"));
                ++iter;
            });

            Assert::IsTrue(iter == endIter, _T("Original container has more data than enumerable container"));
        }


        template <typename tOriginalContainer, typename tDataType = const tOriginalContainer::value_type>
        void TestIEnumerableMixed(tOriginalContainer& rOrigData)
        {
            // Explicitly call the inner to test the implicit cast from an std::container<> to IEnumerable<>
            TestIEnumerableMixedInner<tOriginalContainer, tDataType>(rOrigData, rOrigData);
        }

#pragma endregion

#pragma region Container instances of the template functions
        TEST_METHOD(IEnumerable_CArrays_ToMixedEnumerable)
        {
            int container[]{ 1, 2, 3, 4, 5 };

            TestIEnumerableMixed<decltype(container), int>(container);
        }

        TEST_METHOD(IEnumerable_StdArrays_ToMixedEnumerable)
        {
            std::array<int, 5> container{ 1, 2, 3, 4, 5 };

            TestIEnumerableMixed(container);
        }

        TEST_METHOD(IEnumerable_Vector_ToMixedEnumerable)
        {
            std::vector<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableMixed(container);
        }

        TEST_METHOD(IEnumerable_Deque_ToMixedEnumerable)
        {
            std::deque<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableMixed(container);
        }

        TEST_METHOD(IEnumerable_ForwardList_ToMixedEnumerable)
        {
            std::forward_list<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableMixed(container);
        }

        TEST_METHOD(IEnumerable_List_ToMixedEnumerable)
        {
            std::list<int> container{ 1, 2, 3, 4, 5 };
            TestIEnumerableMixed(container);
        }

        TEST_METHOD(IEnumerable_Set_ToMixedEnumerable)
        {
            std::set<int> container{ 1, 2, 3, 4, 5 };
            // Have to explicitly specify the type, because set returns iterator to const int
            TestIEnumerableMixed(container);
        }

        TEST_METHOD(IEnumerable_Map_ToMixedEnumerable)
        {
            std::map<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestIEnumerableMixed(container);
        }

        TEST_METHOD(IEnumerable_MultiSet_ToMixedEnumerable)
        {
            std::multiset<int> container{ 1, 2, 3, 4, 5 };
            // Have to explicitly specify the type, because multiset returns iterator to const int
            TestIEnumerableMixed(container);
        }

        TEST_METHOD(IEnumerable_MultiMap_ToMixedEnumerable)
        {
            std::multimap<int, double> container{ { 1, 1.0 },{ 2, 2.0 },{ 3, 3.0 },{ 4, 4.0 },{ 5, 5.0 } };
            TestIEnumerableMixed(container);
        }
#pragma endregion

#pragma endregion



    };
}